<?php

$container = require __DIR__ . '/../_app/_bootstrap.php';

$container->getByType(Nette\Application\Application::class)
	->run();
