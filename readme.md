Nette Empty Scaffold AdminLTE Project
===

Requaire
---
* Vagrant centos-php7 or similar

Instalace
---
```shell
git clone git@git.xcroco.com:bare-projects/nette-scaffold-admin-lte.git __PROJECT_NAME__
cd __PROJECT_NAME__
sh init.sh
php clic.php init:project
```

Po vytvoření DB a tabulek
---
Generování všech tabulek do ORM
```shell
php clic.php generator:generate-orm-from-db --all
```
Generování všech tabulek do ORM kromě "table1" a "table2"
```shell
php clic.php generator:generate-orm-from-db --all table1 table2
```
Generování jedné tabulky do ORM
```shell
php clic.php generator:generate-orm-from-db tabulka1
```

Po úpravě ORM (opravy aliasů, přidání childů,...)
---
Generování administrace pro všechny ORM
```shell
php clic.php generator:scaffold-nette-orm --all 
```
Generování administrace pro všechny ORM kromě "User" a "UserRights"
```shell
php clic.php generator:scaffold-nette-orm --all User UserRights
```
Generování administrace pro jeno ORM "User"
```shell
php clic.php generator:scaffold-nette-orm User
```