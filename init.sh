#!/usr/bin/env bash
echo `git submodule update --init`
echo `cp ./_app/config/example.config.local.php ./_app/config/config.local.php`
result=${PWD##*/}"');"
echo `cd _app/config && sed -i "s/__HOST__NAME__');/$result/g" 'config.local.php'`
echo `cd _composer/ && sh installComposerNette.sh`
echo `chmod -R 777 _tmp`
echo `chmod -R 777 _log`
