<?php

define('BASE_DIR', __DIR__ . '/');
define('WWW_DIR', BASE_DIR . 'www/');
define('ADMIN_DIR', BASE_DIR . 'admin/');
define('APP_DIR', BASE_DIR . '_app/');
define('CONF_DIR', APP_DIR . 'config/');
define('CLIC_COMMAND_DIR', APP_DIR . 'commands/');
define('MODULES_DIR', BASE_DIR . '_modules/');
define('COMPOSER_DIR', BASE_DIR . '_composer/');
define('VENDOR_DIR', COMPOSER_DIR . '_vendor/');
define('PHINX_DIR', VENDOR_DIR . 'bin/');
define('TEMP_DIR', BASE_DIR . '_tmp/');
define('LOG_DIR', BASE_DIR . '_log/');
define('ORM_DIR',APP_DIR.'/models/ORM/');
define('ADMIN_MODULE_DIR',APP_DIR.'/AdminModule/');
define('WWW_MODULE_DIR',APP_DIR.'/WwwModule/');

