#!/usr/bin/env php
<?php

if(PHP_SAPI != 'cli'){
	die('This script can be run only from CLI');
}

require_once dirname(__FILE__) . '/_app/_bootstrap.php';

$c = new \App\Lib\CliC();
$c->run(array_slice($argv, 1));
