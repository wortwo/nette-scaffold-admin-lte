<?php

namespace ORM;

use Lib\ImportQueryBuilderDB;
use ObjectRelationMapper\Connector\Database;
use ObjectRelationMapper\ORM;

/**
 * Class ABase
 * @package ORM
 * @property int primaryKey
 */
abstract class ABase extends ORM
{
    /**
     * Construct
     * @param int $primaryKey
     * @throws \ObjectRelationMapper\Exception\ORM
     */
    public function __construct($primaryKey = NULL)
    {
        parent::__construct($primaryKey);
        $this->preGeneratedInfo['allChildAliases'] = array_keys($this->childs);

    }

    protected function setORMStorages()
    {
        $this->configStorage 	= 'ObjectRelationMapper\ConfigStorage\Basic';
        $this->queryBuilder     = new ImportQueryBuilderDB(new Database());
    }

    /**
     * Vrati vsechny aliasy bud v poli nebo spojene pres glue
     * @param null $glue
     * @return string|array
     */
    public function getAllChildAliases($glue = NULL)
    {
        $s = & $this->configStorage;

        if ($glue != NULL) {
            return implode($glue, $this->preGeneratedInfo['allChildAliases']);
        } else {
            return $this->preGeneratedInfo['allChildAliases'];
        }
    }

    /**
     * Vlozi najednou vice orm v jednom dotazu (vhodne pro importy, neloaduje ormka zpet)
     * @param array $loadData
     * @param boolean $withIds
     * @return mixed
     * @throws \ObjectRelationMapper\Exception\ORM
     */
    public function insertMultiple(Array $loadData, $withIds = false)
    {
        if (empty($loadData)) {
            return false;
        }
        if($withIds){
            try{
                $exec = new \Exec();
                $exec->q('ALTER TABLE '.$this->getConfigDbTable().' ADD '.$this->getDbFieldIfExist('oldId').' INT(12) UNSIGNED NULL DEFAULT NULL ;')->ex();
            }catch (\Exception $e){}
        }
        return $this->queryBuilder->insertMultiple($this, $loadData);
    }

    public function getDbFieldIfExist($fieldName, $includeTableName = false)
    {
        $result = $fieldName;
        if (isset($this->aliases[$fieldName])) {
            $result = $this->aliases[$fieldName]->col;
        }
        if ($includeTableName) {
            return $this->getConfigDbTable() . '.' . $result;
        } else {
            return $result;
        }
    }

    public function setSqlParams(Array &$params,$postfix = '')
    {
        foreach ($this as $propertyName => $propertyValue) {
            if(empty($propertyValue) && $propertyValue != 0) continue;
            $params[] = Array(':' . $propertyName . $postfix, $propertyValue);
        }
        return $params;
    }

    /**
     * Vrati string informaci o objektu
     */
    public function __toString()
    {
        ob_start();
        echo get_class($this) . ":\n";
        foreach ($this->getIterator() as $key => $value) {
            echo '   "' . $key . '" = ';
            $this->valueToString($value);
        }
        return ob_get_clean();
    }

    protected function valueToString($mixed,$space = '')
    {
        if(is_object($mixed)){
            if(method_exists($mixed,'toString')){
                $mixed->toString($space);
            }else{
                echo $space.get_class($mixed)."\n";
            }
        }elseif(is_array($mixed)) {
            echo 'Array('.count($mixed).'){'."\n";
            foreach ($mixed as $key => $item) {
                echo '      "' . $key . '" => ';
                $this->valueToString($item);

            }
            echo '      }'."\n";
        }else{
            echo $space.var_dump($mixed);
        }
    }

    /**
     * @param null $loadData
     * @return array
     */
    public static function getForSelect($loadData = NULL)
    {
        $orm = get_called_class();
        /** @var ABase $orm */
        $orm = new $orm();
        $result = array_map(create_function('$o', 'return $o->primaryKey;'), $orm->loadMultiple($loadData));
        return array_combine($result, $result);
    }
}