<?php
/**
 * Class Exec
 * Stejne jako u Query je to tu nastavitelne, EXEC NIC NEFETCHUJE A ANI SE O TO NEPOKOUSI!!!!
 */
class Exec extends \Database\Statement\Exec
{
    protected function setStorage()
    {
        $this->storage = '\Database\Storage\Basic';
        $this->data['server'] = 'master';

	    $this->devel = true;
    }
}