<?php
/**
 * Class Query
 * Krome nastaveni storage se daji pres $this->data->hodnota
 * Nastavit i defaultni moznosti fetche, serveru takze je pak mozno vyplnovat pouze query a parametry
 */
class Query extends \Database\Statement\Query
{
    protected function setStorage()
    {
        $this->storage = '\Database\Storage\Basic';
        $this->data['server'] = 'master';
        $this->data['fetch'] = self::FETCH_ASSOC;

	    $this->devel = true;
    }
}