<?php

namespace Forms;

use Nette\Application\UI,
	Nette\ComponentModel\IContainer,
	\Nette\Forms\Controls;
use ObjectRelationMapper\Search\Search;
use ORM\ABase;
use Tomaj\Form\Renderer\BootstrapInlineRenderer;
use Tomaj\Form\Renderer\BootstrapRenderer;
use Tomaj\Form\Renderer\BootstrapVerticalRenderer;

abstract class ABaseForm extends UI\Form
{
    protected $classArray = ['form-horizontal' => 'form-horizontal'];

	public function __construct(IContainer $parent = NULL, $name = NULL,$labelColWidth=NULL,$inputColWidth=NULL)
	{
		parent::__construct($parent, $name);
        $this->setRenderer(new BootstrapVerticalRenderer());
	}

    public static function unique(Controls\TextBase $control, ABase $orm, $property = null)
    {
        if(!isset($property)){
            $property = $control->getName();
        }
        $value = $control->getValue();
        $search = new Search($orm);
        $search->exact($property,$value);
        return ($search->getCount() == 0);
  }

    public function addLink($name, $label = NULL, $value = NULL, $link = NULL, $linkValue = NULL)
    {
        $controlHidden = new Controls\HiddenField;
        $controlHidden->setDefaultValue($value);
        $this[$name] = $controlHidden;
        $controlLink = new \LinkControl($label, $link, $linkValue, $controlHidden);
        return $this[$name.'Link'] = $controlLink;
    }

    protected function addPlaceholders()
    {
        /** @var Controls\BaseControl $control */
        foreach ($this->getControls() as $control) {
            $placeholder = current($control->getLabel()->getChildren());
            $control->setAttribute('placeholder', $placeholder);
        }
    }

    protected function fillDefaultsFromOrm(\ORM\ABase $orm)
    {
        /** @var Controls\BaseControl $control */
        foreach ($this->getControls() as $control) {
            $control->setDefaultValue($orm->{$orm->getAlias($control->name)});
        }
    }

    protected function addReadOnly(Array $exclude = [],$omitted = false)
    {
        /** @var Controls\BaseControl $control */
        foreach ($this->getControls() as $control) {
            if($control->getParent() instanceof IContainer && in_array($control->getParent()->getName(),$exclude)) continue;
            if(in_array($control->name,$exclude)) continue;
            $control->setAttribute('readonly');
            $control->setOmitted($omitted);
            $this->itemRemoveClass($control,'datepicker');
        }
    }

    protected function itemRemoveClass($item,$class)
    {
        if(!$item instanceof Controls\BaseControl){
            $item = $this->$item;
        }
        $attrs = $item->getControlPrototype()->attrs;
        if(!array_key_exists('class',$attrs)) return $item;
        $class = trim(str_replace($class,'',$attrs['class']));
        $item->setAttribute('class',$class);
        return $item;
    }

    protected function itemAddClass($item,$class)
    {
        if(!$item instanceof Controls\BaseControl){
            $item = $this->$item;
        }
        $oldClass = '';
        $attrs = $item->getControlPrototype()->attrs;
        if(array_key_exists('class',$attrs)){
            $oldClass = $attrs['class'];
        }

        if(strpos($oldClass,$class) === FALSE){
            $item->setAttribute('class',$oldClass.' '.$class);
        }
        return $item;
    }

    /**
     * Adds single-line text input control to the form.
     * @param string $name
     * @param string $label
     * @param string $picker
     * @return \Nette\Forms\Controls\TextInput
     */
    public function addDate($name, $label = NULL, $picker = 'datepicker')
    {
        $control = new Controls\TextInput($label);
        $this->itemAddClass($control,$picker);
        return $this[$name] = $control;
    }

    /**
     * Adds single-line text input control to the form.
     * @param string $name
     * @param string $label
     * @param int $cols
     * @param int $maxLength
     * @return \Nette\Forms\Controls\TextInput
     */
    public function addTimestamp($name, $label = NULL, $cols = NULL, $maxLength = NULL)
    {
        $control = new Controls\DateInput($label, $maxLength);
        $control->setAttribute('size', $cols);
        $this->itemAddClass($control,'datetimepicker');
        return $this[$name] = $control;
    }

    protected function addClass($class)
    {
        $this->classArray[$class] = $class;
    }
}