<?php

namespace Forms;

use Nette\ComponentModel\IContainer;

class LoginForm extends ABaseForm
{
	public function __construct(IContainer $parent = NULL, $name = NULL)
	{
		parent::__construct($parent, $name);

		$this->addText('login', 'Uživatelské jméno')
			->setRequired('Prosím vyplňte své Uživatelské jméno');
		$this->addPassword('password', 'Heslo')
			->setRequired('Prosím vyplňte své Heslo');
		$this->addCheckbox('rememberMe', 'Zapamatovat přihlášení');
		$this->addSubmit('submit', 'Přihlásit se');
	}
}