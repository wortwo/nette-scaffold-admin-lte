<?php

namespace App\WwwModule\Presenters;

use Nette\Application\Responses\FileResponse;
use Nette\Application\UI\Presenter;

abstract class ABasePresenter extends Presenter
{
    /**
     * @var \ORM\User
     */
    protected $loggedUser;

    public function startup()
    {
        parent::startup();
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->siteMenuLinks = [
            'Domů' => 'Default:',
            //ADD_MENU_ITEM
            'Logout' => 'Login:logout',
        ];
    }
}