<?php

namespace Lib;

use Database\Storage\Basic;
use Tracy\Debugger;

class Helper
{
    public static function tracyLogSql()
    {
        $sqlLogger = Debugger::getBar()->getPanel('SqlLogger');
        if(!$sqlLogger instanceof SqlLogger){
            return;
        }
        $queries = Basic::getLastQueries();
        foreach ($queries as $query) {
            $sqlLogger->logQuery($query['query'],$query['params'],$query['time']);
        }
    }
}
