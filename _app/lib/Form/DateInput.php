<?php

namespace Nette\Forms\Controls;

use Nette\InvalidArgumentException;

class DateInput extends TextInput
{
    /** @var bool */
    private $nullable;

    /**
     * Sets control's value.
     * @param  string
     * @return self
     * @internal
     */
    public function setValue($value)
    {
        if ($value === NULL) {
            $value = time();
        } elseif (!is_scalar($value) && !method_exists($value, '__toString')) {
            throw new InvalidArgumentException(sprintf("Value must be scalar or NULL, %s given in field '%s'.", gettype($value), $this->name));
        }elseif(!preg_match('/^[0-9]+$/',$value)){
            $value = strtotime($value);
        }
        $value = date('d.m.Y h:i:s',$value);
        $this->rawValue = $this->value = $value;
        return $this;
    }

    /**
     * Returns control's value.
     * @return string
     */
    public function getValue()
    {
        return $this->nullable && $this->value === '' ? NULL : strtotime($this->value);
    }
}