<?php
namespace Lib;

use \Nette\Security as NS;
use ORM\User;

class DbAuthenticator extends \Nette\Object implements NS\IAuthenticator
{
    protected $ldap;
    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        $user = new User();
        $user->login = $username;
        $user->load();
        if($user->isLoaded() && password_verify($password, $user->password)){
            return new NS\Identity($username, NULL);
        } else {
            throw new NS\AuthenticationException();
        }
    }
}