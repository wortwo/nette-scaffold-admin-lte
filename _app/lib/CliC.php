<?php

namespace App\Lib;

class CliC extends \CliC\CliC
{
    public function __construct()
    {
        parent::__construct();
        $this->addCommandDir(CLIC_COMMAND_DIR);
        $this->setTmpPath(TEMP_DIR);
    }
}