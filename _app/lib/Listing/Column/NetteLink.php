<?php

namespace Listing\Column;

use Listing\Column_Abstract;
use Listing\Column_Interface;
use Listing\Connector_RowInterface;
use Nette\Application\UI\Presenter;


class Column_NetteLink extends Column_Abstract implements Column_Interface
{
    protected $action;
    protected $description;
    protected $type;
    protected $presenter;

    /**
     * Nazev Vstupniho sloupce
     * @param $sourceName
     * @param $action
     * @param \Nette\Application\UI\Presenter $presenter
     * @param $description
     * @param string $type
     */
    public function __construct($sourceName, $action, Presenter $presenter, $description, $type = 'info')
    {
        $this->sourceName = $sourceName;
        $this->action = $action;
        $this->description = $description;
        $this->type = $type;
        $this->presenter = $presenter;
    }

    /**
     * @inheritdoc
     */
    protected function getValue(Connector_RowInterface $source)
    {
        return '<a href="' . $this->presenter->link($this->action, Array(parent::getValue($source))) . '" title="' . $this->description . '" class="btn btn-' . $this->type . ' btn-xs">' . $this->description . '</a>';
    }
}