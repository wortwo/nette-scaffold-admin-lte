<?php

namespace Listing\Column;

use Listing\Column_Abstract;
use Listing\Column_Interface;
use Listing\Connector_RowInterface;
use Nette\Application\UI\Presenter;

class NetteLinkDynamic extends Column_Abstract implements Column_Interface
{
    protected $action;
    protected $descriptionCallback;
    protected $type;
    protected $presenter;
    protected $linkParams;

    /**
     * Nazev Vstupniho sloupce
     * @param $sourceName
     * @param $action
     * @param Presenter $presenter
     * @param $descriptionCallback
     * @param string $type
     */
    public function __construct($sourceName, $action, Presenter $presenter, $descriptionCallback, $type = 'default', $linkParams = [])
    {
        $this->sourceName = $sourceName;
        $this->action = $action;
        $this->descriptionCallback = $descriptionCallback;
        $this->type = $type;
        $this->presenter = $presenter;
        $this->linkParams = $linkParams;
    }

    /**
     * @inheritdoc
     */
    protected function getValue(Connector_RowInterface $source)
    {
        $value = parent::getValue($source);
        $description = call_user_func($this->descriptionCallback, $value);
        $params = $this->linkParams;
        $params[] = $value;
        return '<a href="' . $this->presenter->link($this->action,$params) . '" title="' . $description . '" class="btn btn-' . $this->type . ' btn-xs">' . $description . '</a>';
    }
}