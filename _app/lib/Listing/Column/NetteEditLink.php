<?php

namespace Listing\Column;

use Listing\Column_Abstract;
use Listing\Column_Interface;
use Listing\Connector_RowInterface;
use Nette\Application\UI\Presenter;

class NetteEditLink extends Column_Abstract implements Column_Interface
{
    protected $action;
    protected $presenter;

    /**
     * Nazev Vstupniho sloupce
     * @param $sourceName
     * @param $action
     * @param Presenter $presenter
     */
    public function __construct($sourceName, $action, Presenter $presenter)
    {
        $this->sourceName = $sourceName;
        $this->action = $action;
        $this->presenter = $presenter;
    }

    /**
     * @inheritdoc
     */
    protected function getValue(Connector_RowInterface $source)
    {
        return '<a href="'.$this->presenter->link($this->action, Array(parent::getValue($source))).'" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a>';
    }
}