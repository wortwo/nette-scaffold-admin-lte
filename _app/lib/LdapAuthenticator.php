<?php
namespace Lib;

use Ldap\LDAP;
use Nette\Object;
use Nette\Security as NS;

class LdapAuthenticator extends Object implements NS\IAuthenticator
{
	protected $ldap;

	function __construct(LDAP $ldap)
	{
		$this->ldap = $ldap;
	}

	function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;

		if($this->ldap->authenticate($username, $password)){
			return new NS\Identity($username, NULL);
		} else {
			throw new NS\AuthenticationException('User not found.');
		}
	}
}