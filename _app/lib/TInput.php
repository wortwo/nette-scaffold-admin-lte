<?php

namespace CliC\Base;

/**
 * Class Input
 * @package CliC\Base
 *
 * upgrade: http://stackoverflow.com/questions/11327367/detect-if-a-php-script-is-being-run-interactively-or-not
 */
trait TInput
{
    use Output;

    /**
     * Return input from user console
     * @param string $default Default return value if empty input
     * @return string
     */
    protected function getInput(string $default = ''):string
    {
        if($this->noColor){
            $result = trim(fgets(STDIN));
        }else{
            echo "\e[7m";
            $result = trim(fgets(STDIN));
            echo "\e[0m";
        }

        if($result == '') return $default;
        return $result;
    }

    /**
     * Write message and return input from user console
     * @param string $question Message written to console
     * @param string $defaultAnswer Default return value if empty input
     * @return string
     */
    protected function askUser(string $question, string $defaultAnswer = ''):string
    {
        if(!empty($defaultAnswer)){
            $question .= '[Default => '.$defaultAnswer.']:';
        }
        $this->output($question);
        return $this->getInput($defaultAnswer);
    }

    /**
     * Write message and return one of choices determined by input from user console
     * @param string $question Message written to console
     * @param array $choices Array of returnable choices
     * @return string
     * @throws Exception\RuntimeException if provided with 3 wrong inputs
     */
    protected function askUserChoice(string $question,Array $choices):string
    {
        if(empty($choices)) $this->terminateWithError('Empty choices for method askUserChoice in '.get_called_class());
        reset($choices);
        $returnKey = is_string(key($choices));
        $encoding = 'UTF-8';
        $mb_strtolower = function($name) use ($encoding) {
            return mb_strtolower($name,$encoding);
        };
        $choicesCI = array_map($mb_strtolower,$choices);
        for($i = 0; $i < 3; $i++){
            $this->output($question.' ['.implode('/',$choices).']:');
            $result = array_search(mb_strtolower($this->getInput(NULL),'UTF-8'), $choicesCI);
            if($result !== false){
                if($returnKey) return $result;
                return $choices[$result];
            }
        }
        $this->terminateWithError('User failed to provide valid answer. Terminating...');
    }

    /**
     * Write question and return true|false determined by input from user console
     * @param string $question
     * @return bool
     */
    protected function askUserYesNo(string $question):bool
    {
        return in_array($this->askUserChoice($question,['y','yes','n','no']),['y','yes']);
    }
}