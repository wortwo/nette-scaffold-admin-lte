<?php

namespace Lib;

use Nette\Object;
use Tracy\Debugger;
use Tracy\IBarPanel;

class SqlLogger extends Object implements IBarPanel
{

    /**
     * @var mixed instance
     */
    private static $_instance = null;

    /** @var array */
    private $queries = array();

    /**
     * Instantiate using {@link getInstance()}; Diggriola\Panel is a singleton object.
     * @return SqlLogger
     */
    public function __construct()
    {
    }


    /**
     * Enforce singleton. Disallow cloning.
     * @return void
     */
    private function __clone()
    {
    }


    /**
     * Create singleton instance
     *
     * @return SqlLogger
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }


    /**
     * @param string
     * @param array
     * @param array
     */
    public function logQuery($sql, array $params = NULL, $queryTime = null)
    {
        $this->queries[] = array(
            'sql' => $sql,
            'params' => $params,
            'time' => $queryTime);
    }


    public function getTab()
    {
        return '<span title="Doctrine 2">'
        . '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEYSURBVBgZBcHPio5hGAfg6/2+R980k6wmJgsJ5U/ZOAqbSc2GnXOwUg7BESgLUeIQ1GSjLFnMwsKGGg1qxJRmPM97/1zXFAAAAEADdlfZzr26miup2svnelq7d2aYgt3rebl585wN6+K3I1/9fJe7O/uIePP2SypJkiRJ0vMhr55FLCA3zgIAOK9uQ4MS361ZOSX+OrTvkgINSjS/HIvhjxNNFGgQsbSmabohKDNoUGLohsls6BaiQIMSs2FYmnXdUsygQYmumy3Nhi6igwalDEOJEjPKP7CA2aFNK8Bkyy3fdNCg7r9/fW3jgpVJbDmy5+PB2IYp4MXFelQ7izPrhkPHB+P5/PjhD5gCgCenx+VR/dODEwD+A3T7nqbxwf1HAAAAAElFTkSuQmCC" />'
        . count($this->queries) . ' queries'
        . '</span>';
    }

    /**
     * @param array
     * @return string
     */
    protected function processQuery(array $query)
    {
        $s = '';
        $sql = isset($query['sql']) ? $query['sql'] : 'a';
        foreach ($query['params'] AS $name => $par) {
            $sql = str_replace($name, "'".$par."'", $sql);
        }
//		$params = isset($query['params']) ? $query['params'] : 'b';
        $s .= '<tr>';
        $s .= '<td>' . (round(($query['time'] * 1000), 3)) . '</td>';
        $s .= '<td class="nette-Doctrine2Panel-sql">' . \Nette\Database\Helpers::dumpSql($sql).'</td>';
//		$s .= '<td>' . Nette\Diagnostics\Dumper::toHtml($params) . '</td></tr>';
        $s .= '</tr>';

        return $s;
    }

    protected function renderStyles()
    {
        return '<style> #nette-debug td.nette-Doctrine2Panel-sql { background: white !important }
			#nette-debug .nette-Doctrine2Panel-source { color: #BBB !important }
			#nette-debug nette-Doctrine2Panel tr table { margin: 8px 0; max-height: 150px; overflow:auto } </style>';
    }

    /**
     * @param \PDOException
     * @return array
     */
    public function renderException($e)
    {
        if ($e instanceof \PDOException && count($this->queries)) {
            $s = '<table><tr><th>Time&nbsp;ms</th><th>SQL</th><th>Params</th></tr>';
            $s .= $this->processQuery(end($this->queries));
            $s .= '</table>';
            return array(
                'tab' => 'SQL',
                'panel' => $this->renderStyles() . '<div class="nette-inner nette-Doctrine2Panel">' . $s . '</div>',
            );
        } else {
            \Nette\Bridges\DatabaseTracy\ConnectionPanel::renderException($e);
        }
    }

    public function getPanel()
    {
        $s = '';
        foreach ($this->queries as $query) {
            $s .= $this->processQuery($query);
        }

        return empty($this->queries) ? '' :
            $this->renderStyles() .
            '<h1>Queries: ' . count($this->queries) . '</h1>
			<div class="nette-inner nette-Doctrine2Panel">
			<table>
			<tr><th>Time&nbsp;ms</th><th>SQL</th>
			</tr>' . $s . '
			</table>
			</div>';
    }

    /**
     * @return \Nette\Bridges\DatabaseTracy\ConnectionPanel
     */
    public static function register()
    {
        $panel = new static;
        \Tracy\Debugger::getBar()->addPanel($panel,'SqlLogger');
        return $panel;
    }
}
