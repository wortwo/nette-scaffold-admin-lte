<?php

namespace Lib;

use Nette\Object;
use Nette\Security as NS;
use Nette\Security\AuthenticationException;

class MultiAuthenticator extends Object implements NS\IAuthenticator
{
    protected $authenticators = Array();
    function addAuthenticator(NS\IAuthenticator $authenticator)
    {
        $this->authenticators[] = $authenticator;
    }
    function authenticate(array $credentials)
    {
        list($username, $password) = $credentials;
        foreach($this->authenticators as $authenticator){
            try{
                $auth = $authenticator->authenticate(Array($username, $password));
                if($auth instanceof NS\Identity){
                    return $auth;
                }
            } catch (AuthenticationException $e){
                continue;
            }
        }
        throw new MultiAuthenticationException('User not found.');
    }
}
class MultiAuthenticationException extends \Exception {}