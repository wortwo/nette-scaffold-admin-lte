<?php

namespace Lib;

use ObjectRelationMapper\Base\AORM;
use ObjectRelationMapper\QueryBuilder\DB;

class ImportQueryBuilderDB extends DB
{
    /**
     * @inheritdoc
     */
    public function insertMultiple(AORM $orm, Array $data, $withIds = false)
    {
        $columns = $orm->getAllDbFields();
        if(!$withIds){
            $columns = array_diff($columns, Array($orm->getConfigDbPrimaryKey()));
        }
        $query = 'INSERT INTO ' . $orm->getConfigDbTable() . '  ';
        $query .= '(' . implode(',', $columns) . ')';

        $i = 0;
        $values = Array();
        $params = Array();
        foreach ($data as $singleOrm) {
            $cols = Array();
            foreach ($columns as $column) {
                $cols[] = ':' . $i . $column;
                $params[] = Array(':' . $i . $column, $singleOrm->{$orm->getAlias($column)});
            }

            $values[] = '(' . implode(',', $cols) . ')';
            $i++;
        }

        $query .= ' VALUES ' . implode(', ', $values);
        return $this->connector->exec($query, $params, $orm->getConfigDbServer());
    }
}