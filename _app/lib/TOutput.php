<?php

namespace CliC\Base;

/**
 * Class TOutput
 * @package CliC\Base
 */
trait TOutput
{
    protected $progressIndicator = Array('|', '/', '-', '\\', '|', '/', '-', '\\');
    protected $workingSent = false;
    protected $lastOutputLgt = 0;
    /** @var float $progress  */
    protected $progress = 0;
    /** @var float $step  */
    protected $step = 0;

    protected function dashesToCamelCase(string $string):string
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', str_replace(' ', '\\', ucwords(str_replace(Array('/', ':'), Array(' ', ' '), $string))))));
    }

    protected function sendOk()
    {
        $this->output(" .... [ <green>OK</green> ]");
    }

    protected function sendSkipped()
    {
        $this->output(' .... [ <yellow>SKIPPED</yellow> ]');
    }

    protected function sendWarning()
    {
        $this->output(' .... [ <yellow>WARNING</yellow> ]');
    }

    protected function sendFail()
    {
        $this->output(" .... [ <red>FAIL</red> ]");
    }

    protected function sendFailExplain(string $message)
    {
        $this->sendFail();
        $this->output(" " . $message);
    }

    protected function sendErrorMessage(string $message)
    {
        $this->outputLn();
        $this->outputLn(" <redbg>  " . str_repeat(' ', mb_strlen($message)) . "  </redbg>");
        $this->outputLn(" <redbg>  " . $message . "  </redbg>");
        $this->outputLn(" <redbg>  " . str_repeat(' ', mb_strlen($message)) . "  </redbg>");
        $this->outputLn();
    }

    protected function sendSuccessMessage(string $message)
    {
        $this->outputLn();
        $this->outputLn(" <greenbg>  " . str_repeat(' ', mb_strlen($message)) . "  </greenbg>");
        $this->outputLn(" <greenbg>  " . $message . "  </greenbg>");
        $this->outputLn(" <greenbg>  " . str_repeat(' ', mb_strlen($message)) . "  </greenbg>");
        $this->outputLn();
    }

    /**
     * @param string $message
     * @throws Exception\RuntimeException
     */
    protected function terminateWithError(string $message)
    {
        $this->sendErrorMessage($message);
        throw new Exception\RuntimeException($message);
    }

    protected function outputHeadlineLn(string $output, string $colour = 'green')
    {
        $this->outputLn("<$colour>$output</$colour>");
        $this->outputLn("<$colour>" . str_repeat('-', strlen($output)) . "</$colour>");
    }

    protected function sendWorking()
    {
        $this->workingSent = true;
        $op = next($this->progressIndicator);
        if($op == false){
            reset($this->progressIndicator);
            $op = current($this->progressIndicator);
        }
        $this->output("\033[1D");
        $this->output($op);
    }

    protected function resetWorking()
    {
        if($this->workingSent){
            $this->output("\033[1D");
            $this->workingSent = false;
        }
    }

    protected function output(string $output = '')
    {
        if (PHP_SAPI == 'cli' && !$this->webOutput) {
            $translator = new \CliC\Base\Output\Console();
        } else {
            $translator = new \CliC\Base\Output\Web();
        }

        if ($this->noColor == false) {
            $output = $translator->translate($output);
        } else {
            $output = $translator->removeTags($output);
        }

        $this->lastOutputLgt = strlen($output);
        echo $output;
    }

    protected function outputLn(string $output = '')
    {
        $this->output($output . PHP_EOL);
    }

    protected function outputPadded(string $output = '', int $paddingLength = 0)
    {
        $this->output(str_pad($output, $paddingLength));
    }

    protected function outputPaddedLn(string $output = '', int $paddingLength = 0)
    {
        $this->outputLn(str_pad($output, $paddingLength));
    }

    /**
     * Delete last message written by output method
     */
    protected function removeLastOutput()
    {
        if($this->lastOutputLgt > 0){
            $this->output("\033[{$this->lastOutputLgt}D".str_repeat(' ', $this->lastOutputLgt)."\033[{$this->lastOutputLgt}D");
        }
    }

    /**
     * Create progressbar in % with message from parameter
     * @param int $numberOfOperations
     * @param string $message
     */
    public function progressStart(int $numberOfOperations, string $message = 'Progress')
    {
        $this->progress = 0;
        $numberOfOperations = (int)$numberOfOperations;
        if ($numberOfOperations != 0) {
            $this->step = 1 / ($numberOfOperations / 100);
        }
        $this->output("<yellow>$message</yellow><green>     0 %</green>");
        if ($this->step == 0) {
            $this->step = 100;
            $this->progressAddStep();
        }
    }

    /**
     * Add step to progressbar and refresh its value
     * @return bool
     */
    public function progressAddStep():bool
    {
        $this->progress += $this->step;
        if ($this->progress >= 100) {
            $this->outputLn("\033[5D<green>" . str_pad(ceil(100), 3, ' ', STR_PAD_LEFT) . " %</green>");
            return false;
        } else {
            $this->output("\033[5D<green>" . str_pad(ceil($this->progress), 3, ' ', STR_PAD_LEFT) . " %</green>");
        }
        return true;
    }
}