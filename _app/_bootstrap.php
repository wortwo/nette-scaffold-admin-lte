<?php

use Tracy\Debugger;

umask(0);
setlocale(LC_ALL, 'cs_CZ.UTF-8');
date_default_timezone_set('Europe/Prague');
ini_set('memmory_limit','672M');
set_time_limit(100);
require_once __DIR__.'/../paths.php';
require_once CONF_DIR.'config.local.php';
require VENDOR_DIR.'autoload.php';

$configurator = new Nette\Configurator;
$configurator->setTempDirectory(TEMP_DIR);
$configurator->setDebugMode(IS_DEVEL);
$configurator->enableDebugger(LOG_DIR);

$configurator->addConfig(CONF_DIR . '/config.neon');
if(IS_DEVEL){
    $configurator->addConfig(CONF_DIR . '/config.devel.neon');
}else{
    $configurator->addConfig(CONF_DIR . '/config.production.neon');
}

Debugger::timer();
Debugger::$strictMode = true;
Debugger::$showLocation = true;
Debugger::$maxLen = 1999;

$loader = $configurator->createRobotLoader()
    ->addDirectory(APP_DIR)
    ->addDirectory(MODULES_DIR)
    ->addDirectory(VENDOR_DIR);
$loader->ignoreDirs .= ', _tests, _doc, tests, Examples, Documentation, unitTests, _logs, _tmp, fonts';
$loader->autoRebuild = true;
$loader->register();

\Tracy\Debugger::timer('page-load');
\Lib\SqlLogger::register();

$container = $configurator->createContainer();

$container->getService('application')->onError[] = function ($sender, $exception) {
    \Tracy\Debugger::log($exception, \Tracy\Debugger::ERROR);
};
$parameters = $container->getParameters();
foreach($parameters['database'] as $alias => $config){
    $dsn = new \Database\Configuration\Server();
    $dsn->host($config['host'])
        ->database($config['name'])
        ->user($config['user'])
        ->password($config['pass'])
        ->alias($alias)
        ->keepalive($config['keep'])
        ->persistent($config['pers']);
    $connection = new \Database\Connector\PDO($dsn);
    \Database\Storage\Basic::addConnection($connection);
}

$multiAuthenticator = new \Lib\MultiAuthenticator();
$multiAuthenticator->addAuthenticator(new \Lib\DbAuthenticator());
$container->getService('user')->setAuthenticator($multiAuthenticator);

/**
 * Tracy\Debugger::dump() shortcut.
 * @tracySkipLocation
 */
function dump_die()
{
    call_user_func_array('dump',func_get_args());
    \Lib\Helper::tracyLogSql();
    die();
}

return $container;
