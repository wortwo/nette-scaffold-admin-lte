<?php

namespace Tables;

abstract class Table extends \Listing\Table
{
    protected $classes = Array('table','table-bordered','table-striped','dataTable');

    /**
     * Upravy columny do citelne podoby
     * @throws \Exception
     */
    public function translateData()
    {
        parent::translateData();
    }
}