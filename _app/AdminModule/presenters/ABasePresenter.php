<?php

namespace App\AdminModule\Presenters;

use Nette\Application\Responses\FileResponse;
use Nette\Application\UI\Presenter;

abstract class ABasePresenter extends Presenter
{
    /**
     * @var \ORM\User
     */
    protected $loggedUser;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isLoggedIn()) {
            $s = $this->getSession('redirect');
            $s->loginRedirect = $this->storeRequest();
            $this->redirect('Login:default');
        } else {
            if (!$this->loggedUser instanceof \ORM\User) {
                $user = new \ORM\User();
                $user->login = $this->user->getId();
            } else {
                $user = $this->loggedUser;
            }
            $user->load();
            $this->loggedUser = $user;
            $this->template->loggedUser = $this->loggedUser;
        }
    }

    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->siteMenuLinks = [
            'Domů' => 'Default:',
            'User' => 'User:',
            //ADD_MENU_ITEM
            'Logout' => 'Login:logout',
        ];
    }

    protected function exportListing(\Listing\Table $listing, $listable = true)
    {
        $format = $this->getParameter('exportFormat');
        if($listable) $listing->addClass('listtable');
        $listing->addClass('exportable');
        if(empty($format)){
            return;
        }
        $classes = $listing->getClasses();
        $formats = [
            'Listing\Column_Date' => \TableOutput\Column\Column::OUTPUT_DATE,
        ];

        //HEADERS
        $headers = [];
        $headersSet = false;

        //DATA
        $listing->translateData();
        $tableData = [];
        $valueFormats = [];
        /** @var \Listing\Internal_Row $data */
        foreach ($listing->getTableData() as $row => $data) {
            /** @var \Listing\Connector_RowInterface $source */
            $source = $data->getSource();
            /** @var \Listing\Column_Abstract $column */
            foreach ($data as $column) {
                if(!$headersSet){
                    $headers[] = $column->getHeaderText();
                }
                $tableData[$row][$column->getSourceName()] = strip_tags($column->translate($source));
                if(array_key_exists(get_class($column),$formats)){
                    $valueFormats[$column->getSourceName()] = $formats[get_class($column)];
                }else{
                    $valueFormats[$column->getSourceName()] = \TableOutput\Column\Column::OUTPUT_DEFAULT;
                }
            }
            $headersSet = true;
            if(in_array('remove-action-column',$classes)){
                array_pop($tableData[$row]);
                array_pop($headers);
            }
        }

        $table = new \TableOutput\Table();
        $table->setAllColumnsSetting('fontSize',6.5)
            ->setAllColumnsSetting('pdf_margin_left',15)
            ->setAllColumnsSetting('pdf_margin_right',5)
            ->setAllColumnsSetting('pdf_margin_top',5)
            ->setAllColumnsSetting('pdf_margin_bottom',5)
            //->setAllColumnsSetting('pdf_width_percent',false)
            //->setAllColumnsSetting('height',4)
            ->setHeaderLangs(array_combine(array_keys($tableData[0]),$headers))
            ->calculateWidth()
            ->addData($tableData)
            ->setColumnValueFormats($valueFormats);

        if(!file_exists(TEMP_DIR.'Download')){
            mkdir(TEMP_DIR.'Download');
        }
        $file = TEMP_DIR.'Download/'.uniqid('DL').'.'.strtolower($format);
        file_put_contents($file,$table->render($format));
        $this->sendResponse(new FileResponse($file, 'Export.'.strtolower($format), mime_content_type($file)));
    }
}