<?php

namespace App\AdminModule\Presenters;

use Forms\LoginForm;
use Ldap\LDAP;
use Lib\MultiAuthenticationException;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;
use Nette\Security\AuthenticationException;
use ObjectRelationMapper\Search\Search;
use ORM\User;

class LoginPresenter extends Presenter
{
    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->siteMenuLinks = [];
    }

    /**
     * @return LoginForm
     */
    protected function createComponentLoginForm()
	{
		$form = new LoginForm();
		$form->onSuccess[] = [$this, 'authenticateUserForm'];
		return $form;
	}

    public function authenticateUser($username,$password,$rememberMe = false)
    {
        try {
            $this->user->login($username, $password);

            $u = new User();
            $u->login = $username;
            $u->load();
            if (!$u->isLoaded()) {
                $ldap = new LDAP();
                $username = preg_replace('/@.*/', '', $username);
                if(!$ldap->authenticate($username, $password)){
                    throw new MultiAuthenticationException();
                }
                $u->login = $username;
                $u->password = password_hash($password, PASSWORD_DEFAULT);
                $u->email = $username.'@xcroco.com';
                $u->created = time();
                $u->save();
            }
            if($rememberMe){
                $this->getUser()->setExpiration('+ 30 days');
            }
        } catch (MultiAuthenticationException $e) {
            return ['type' => 'danger', 'message' => 'Bad username or password'];
        }
        return ['type' => 'success','message' => 'Logged in successfully'];
    }
    public function authenticateUserForm(Form $form)
    {
        $v = $form->getValues();
        $result = $this->authenticateUser($v->login,$v->password,$v->rememberMe);
        $this->flashMessage($result['message'],$result['type']);

        $s = $this->getSession('redirect');
        if(isset($s->loginRedirect) && !empty($s->loginRedirect)){
            $request = $s->loginRedirect;
            unset($s->loginRedirect);
            $this->restoreRequest($request);
        } else {
            $this->redirect('User:workPlan');
        }
    }

	public function renderLogout()
	{
		$this->user->logout(true);
		$this->redirect('Login:default');
	}

	public function renderDefault(){}
}
