<?php

namespace CliC\Command\Init;

use \CliC\Base\ICommand;
use CliC\Base\Input;
use Forms\LoginForm;
use Listing;
use Nette\DI\Config\Loader;
use ObjectRelationMapper\Connector\PDO;
use ORM\ABase;

/**
 * Class Project
 * @package CliC\Command
 *
 * @property bool devel
 * @property bool production
 */
class Project extends \CliC\Base\ACommand implements ICommand
{
    use Input;

    public function __construct(\CliC\CliC $cliC)
    {
        parent::__construct($cliC);
        $this->addArgument('--devel', 'devel', true);
        $this->addArgument('--production', 'production', true);
    }

    public function main()
    {
        $this->outputHeadlineLn('Project inicialization');

        if($this->devel){
            if (!IS_DEVEL) {
                $this->terminateWithError('Can be run only in devel environment!');
            }

            $this->outputLn('<yellow>Setting config.devel.neon:</yellow>');
            $content = file_get_contents(CONF_DIR.'config.devel.neon');
            $configValues = [];
            $this->askUserConfigValue($configValues,$content,'host','DB host?','localhost');
            $this->askUserConfigValue($configValues,$content,'user','DB user name?','root');
            $this->askUserConfigValue($configValues,$content,'pass','DB password?','celer4000');
            $suggested = preg_replace('/\.[^.\s]+$/', '', basename(BASE_DIR));
            $dbName = $this->askUserConfigValue($configValues,$content,'name','DB name?',$suggested);
            $this->setConfigValues($content,$configValues);
            file_put_contents(CONF_DIR.'config.devel.neon',$content);
            if($this->askUserYesNo('Do you want to bootstrap phinx?')){
                echo `php clic.php migrations:bootstrap`;
            }
            if($this->askUserYesNo('Do you want to create database?')){
                $this->outputLn('Creating DB '.$dbName);
                $loader = new Loader();
                $config = $loader->load(CONF_DIR . 'config.devel.neon')['parameters']['database']['master'];
                // Create connection
                $conn = new \mysqli($config['host'], $config['user'], $config['pass']);
                if ($conn->connect_error) {
                    $this->terminateWithError('Connection failed: '. $conn->connect_error);
                }
                $sql = 'CREATE DATABASE IF NOT EXISTS `'.$dbName.'` CHARACTER SET utf8 COLLATE utf8_unicode_ci;';
                if ($conn->query($sql) === TRUE) {
                    $this->outputLn('<green>Database created successfully</green>');
                } else {
                    $this->terminateWithError('Error creating database: ' . $conn->error);
                }
                if($this->askUserYesNo('Do you want to create basic User table?')){
                    $this->createUserMigration();
                    echo `php clic.php migrations:migrate`;
                    echo `php clic.php generator:generate-orm-from-db user --col-prefix=u_`;
                }
                $conn->close();
            }
        }
        if($this->production){
            $this->outputLn('<yellow>Setting config.production.neon:</yellow>');
            $content = file_get_contents(CONF_DIR.'config.production.neon');
            $configValues = [];
            $userMail = `git config user.email`;
            $this->askUserConfigValue($configValues,$content,'email','Error email?',$userMail);
            if(!$this->askUserYesNo('Do you want to set DB settings for production?')){
                $this->setConfigValues($content,$configValues);
                file_put_contents(CONF_DIR.'config.production.neon',$content);
                $this->outputLn('<green>Inicialization completed.</green> <red>Please keep in mind, that config.production.neon is probably not set properly.</red>');
                return;
            }
            $this->askUserConfigValue($configValues,$content,'host','DB host?','192.168.100.1');
            $this->askUserConfigValue($configValues,$content,'user','DB user name?','');
            $this->askUserConfigValue($configValues,$content,'pass','DB password?','');
            $suggested = preg_replace('/\.[^.\s]+$/', '', basename(BASE_DIR));
            $this->askUserConfigValue($configValues,$content,'name','DB name?',$suggested);
            $this->setConfigValues($content,$configValues);
            file_put_contents(CONF_DIR.'config.production.neon',$content);
        }
        $this->outputLn('<green>Inicialization completed.</green>');
        return;
    }

    protected function createUserMigration()
    {
        $migrationContent = "<?php

use Phinx\\Migration\\AbstractMigration;

class UserTable extends AbstractMigration
{
    public function up()
    {
        \$this->execute(\"
        CREATE TABLE IF NOT EXISTS `user` (
          `u_id` int(10) unsigned NOT NULL,
          `u_name` varchar(200) CHARACTER SET utf8 NOT NULL,
          `u_login` varchar(200) CHARACTER SET utf8 NOT NULL,
          `u_password` varchar(200) CHARACTER SET utf8 NOT NULL,
          `u_created` int(10) unsigned NOT NULL,
          `u_email` varchar(200) CHARACTER SET utf8 DEFAULT NULL
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        
        ALTER TABLE `user`
          ADD PRIMARY KEY (`u_id`);
        
        ALTER TABLE `user`
          MODIFY `u_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
        \");
    }
    
    public function down()
    {
        \$this->execute(\"DROP TABLE `user`;\");
    }
}";
        file_put_contents(BASE_DIR.'_migrations/'.date('YmdHis').'_user_table.php',$migrationContent);
    }

    protected function askUserConfigValue(Array &$configValues,$configContent,$config,$question,$default)
    {
        if(preg_match('/'.$config.':\s*([^\s]+)/',$configContent,$match)){
            if(is_array($default)){
                return $configValues[$match[1]] = $this->askUserChoice($question,$default);
            }else{
                return $configValues[$match[1]] = $this->askUser($question,$default);
            }
        }
    }

    protected function setConfigValues(&$content,$values)
    {
        foreach ($values as $search => $replace) {
            $content = str_replace($search,$replace,$content);
        }
    }

    public function getName()
    {
        return 'project';
    }

    public function getDescription()
    {
        return 'Project config settings';
    }

    public function writeHelp()
    {
        $this->outputLn("<red>General Info:</red>");
        $this->outputLn("<red>-------------</red>");
        $this->outputLn("<yellow>" . $this->getDescription() . "</yellow>");
        $this->outputLn("");
        $this->outputLn("<red>Available options:</red>");
        $this->listAvailableOptions();
        $this->outputLn(str_pad("<DB table name>", 25) . "<cyan>One or more orm names separated by space</cyan>");
        $this->outputLn("");
    }
}