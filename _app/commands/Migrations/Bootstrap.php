<?php

namespace CliC\Command\Migrations;
use CliC\Base\ACommand;
use \CliC\Base\ICommand;
use Nette\DI\Config\Loader;

class Bootstrap extends ACommand implements ICommand
{
	protected $scheduledTasks = Array();
	protected $config;

	public function getName()
	{
		return 'bootstrap';
	}

	public function getDescription()
	{
		return 'Get Migrations ready for use';
	}

	public function writeHelp()
	{
		parent::writeHelp();
	}

	public function main()
	{
		$baseDir = BASE_DIR;
		$phinxDir = PHINX_DIR;

		$loader = new Loader();
		$this->config = $loader->load(CONF_DIR . 'config.devel.neon')['parameters']['database']['master'];
		$phinxYml = '
paths:
    migrations: %%PHINX_CONFIG_DIR%%/migrations

environments:
    default_migration_table: phinxlog
    default_database: development
    production:
        adapter: mysql
        host: '.$this->config['host'].'
        name: '.$this->config['name'].'
        user: '.$this->config['user'].'
        pass: "'.$this->config['pass'].'"
        port: 3306

    development:
        adapter: mysql
        host: '.$this->config['host'].'
        name: '.$this->config['name'].'
        user: '.$this->config['user'].'
        pass: "'.$this->config['pass'].'"
        port: 3306

    testing:
        adapter: mysql
        host: '.$this->config['host'].'
        name: '.$this->config['name'].'
        user: '.$this->config['user'].'
        pass: "'.$this->config['pass'].'"
        port: 3306';

		file_put_contents(PHINX_DIR . 'phinx.yml', $phinxYml);

        if(!file_exists(PHINX_DIR.'migrations')) echo `ln -s $baseDir/_migrations $phinxDir/migrations`;

		echo `php $baseDir/clic.php migrations:status`;
	}

}