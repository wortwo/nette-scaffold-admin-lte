<?php

namespace CliC\Command\Migrations;
use CliC\Base\ACommand;
use \CliC\Base\ICommand;

class Rollback extends ACommand implements ICommand
{
	protected $scheduledTasks = Array();
	protected $config;

	public function getName()
	{
		return 'rollback';
	}

	public function getDescription()
	{
		return 'Rolls back a single last migration';
	}

	public function writeHelp()
	{
		parent::writeHelp();
	}

	public function main()
	{
        $phinxDir = PHINX_DIR;
		echo `( cd $phinxDir && ./phinx rollback )`;
	}
}