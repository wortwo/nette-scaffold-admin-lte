<?php

namespace CliC\Command\Migrations;
use CliC\Base\ACommand;
use \CliC\Base\ICommand;

class Migrate extends ACommand implements ICommand
{
	protected $scheduledTasks = Array();
	protected $config;

	public function getName()
	{
		return 'migrate';
	}

	public function getDescription()
	{
		return 'Performs all available migrations and prints out output from them';
	}

	public function writeHelp()
	{
		parent::writeHelp();
	}

	public function main()
	{
        $phinxDir = PHINX_DIR;
		echo `( cd $phinxDir && ./phinx migrate )`;
	}
}