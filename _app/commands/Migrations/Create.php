<?php

namespace CliC\Command\Migrations;
use CliC\Base\ACommand;
use \CliC\Base\ICommand;

class Create extends ACommand implements ICommand
{
    protected $scheduledTasks = Array();
    protected $config;

    public function getName()
    {
        return 'create';
    }

    public function getDescription()
    {
        return 'Creates a new migration file. It requires one argument and that is the name of the migration. The migration name should be specified in CamelCase format.';
    }

    public function writeHelp()
    {
        parent::writeHelp();
    }

    public function main()
    {
        $name = $this->filterUnrecognizedOptions();
        $name = array_shift($name);
        if(!preg_match('/^([A-Z]{1}[a-z]*)+$/',$name)){
            $this->terminateWithError('Argument is the name of the migration. The migration name should be specified in CamelCase format.');
        }
        $phinxDir = PHINX_DIR;
        echo `( cd $phinxDir && ./phinx create "$name")`;
    }
}