<?php

namespace CliC\Command\Migrations;
use CliC\Base\ACommand;
use \CliC\Base\ICommand;

class Status extends ACommand implements ICommand
{
	protected $scheduledTasks = Array();
	protected $config;

	public function getName()
	{
		return 'status';
	}

	public function getDescription()
	{
		return 'Prints out migration status';
	}

	public function writeHelp()
	{
		parent::writeHelp();
	}

	public function main()
	{
        $phinxDir = PHINX_DIR;
		echo `( cd $phinxDir && ./phinx status )`;
	}
}