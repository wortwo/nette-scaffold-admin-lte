<?php

namespace CliC\Command\Generator;

use \CliC\Base\ICommand;
use CliC\Base\Input;
use Forms\LoginForm;
use Listing;
use ORM\ABase;

/**
 * Class ScaffoldNetteOrm
 * @package CliC\Command
 *
 * @property bool all
 * @property bool addMenu
 * @property string formPath
 * @property string superadminForms
 * @property string ormPath
 * @property string tablePath
 * @property string templatePath
 * @property string presenterPath
 */
class ScaffoldNetteOrm extends \CliC\Base\ACommand implements ICommand
{
    use Input;
    protected $addCreatedTimestamp = false;
    protected $addUpdatedTimestamp = false;

    public function __construct(\CliC\CliC $cliC)
    {
        parent::__construct($cliC);
        $this->addArgument('--all', 'all', false);
        $this->addArgument('--addMenu', 'addMenu', true);
        $this->addArgument('--superadminForms', 'superadminForms', true);
        $this->addArgument('--form-path=(.*)', 'formPath', ADMIN_MODULE_DIR.'forms/');
        $this->addArgument('--orm-path=(.*)', 'ormPath', APP_DIR.'models/ORM/');
        $this->addArgument('--table-path=(.*)', 'tablePath', ADMIN_MODULE_DIR.'tables/');
        $this->addArgument('--template-path=(.*)', 'templatePath', ADMIN_MODULE_DIR.'templates/');
        $this->addArgument('--presenter-path=(.*)', 'presenterPath', ADMIN_MODULE_DIR.'presenters/');
    }

    public function main()
    {
        if (!IS_DEVEL) {
            $this->terminateWithError('Can be run only in devel environment!');
        }

        $this->outputHeadlineLn('Creating scaffolds for:','yellow');
        $ormNames = $this->filterUnrecognizedOptions();
        if($this->all){
            $all = [];
            $ormNames[] = 'ABase';
            foreach(glob($this->ormPath.'*.php') as $file) {
                $file = str_replace($this->ormPath,'',substr($file, 0, -4));
                if(in_array($file,$ormNames)) continue;
                $all[] = $file;
            }
            $ormNames = $all;
        }
        $reflections = [];
        foreach ($ormNames as $orm) {
            $class = '\ORM\\'.$orm;
            if(!class_exists($class)) continue;
            $ref = new \ReflectionClass($class);

            if(!$ref->isInstantiable() || !$ref->isSubclassOf('\ORM\ABase')) continue;
            $reflections[] = $ref;
            $this->outputLn('   <blue>'.$class.'</blue>');
        }

        if(empty($reflections)) $this->terminateWithError('No orm found! Check paths and orm names.');

        $this->output('<yellow>Checking paths</yellow>...');
        if (!file_exists($this->formPath) && !mkdir($this->formPath, 0777, true)) {
            $this->terminateWithError('Path for FORMS couldn\'t be created. "'.$this->formPath.'"');
        }
        if (!file_exists($this->tablePath) && !mkdir($this->tablePath, 0777, true)) {
            $this->terminateWithError('Path for TABLES couldn\'t be created. "'.$this->tablePath.'"');
        }
        if (!file_exists($this->templatePath) && !mkdir($this->templatePath, 0777, true)) {
            $this->terminateWithError('Path for TEMPLATES couldn\'t be created. "'.$this->templatePath.'"');
        }
        if (!file_exists($this->presenterPath) && !mkdir($this->presenterPath, 0777, true)) {
            $this->terminateWithError('Path for PRESENTERS couldn\'t be created. "'.$this->presenterPath.'"');
        }
        $this->outputLn('<green>OK</green>');

        /** @var \ReflectionClass $reflection */
        foreach ($reflections as $reflection) {
            $this->addCreatedTimestamp = false;
            $this->addUpdatedTimestamp = false;
            $this->outputHeadlineLn($reflection->getShortName().' scaffold - BEGIN');
            $this->output('<yellow>Form...</yellow>');
            if($this->createForm($reflection)) $this->outputLn('<green>OK</green>');
            $this->output('<yellow>Presenter...</yellow>');
            if($this->createPresenter($reflection)) $this->outputLn('<green>OK</green>');
            $this->output('<yellow>Table...</yellow>');
            if($this->createTable($reflection)) $this->outputLn('<green>OK</green>');
            $this->output('<yellow>Templates...</yellow>');
            if($this->createTemplates($reflection)) $this->outputLn('<green>OK</green>');
            if($this->addMenu){
                $this->output('<yellow>Adding to menu...</yellow>');
                if($this->createMenu($reflection)) $this->outputLn('<green>OK</green>');
            }
            $this->outputHeadlineLn($reflection->getShortName().' scaffold - DONE');
        }
    }

    protected function createTemplates(\ReflectionClass $reflectionClass)
    {
        $dir = $this->templatePath.'/'.$reflectionClass->getShortName().'/';
        if (!file_exists($dir) && !mkdir($dir, 0777, true)) {
            $this->terminateWithError('Path for TEMPLATES couldn\'t be created. "'.$dir.'"');
        }
        file_put_contents($dir.'default.latte',$this->getPreparedTemplate($reflectionClass,'default'));
        file_put_contents($dir.'detail.latte',$this->getPreparedTemplate($reflectionClass,'detail'));
        return true;
    }

    protected function createMenu(\ReflectionClass $reflectionClass)
    {
        if(!file_exists($this->presenterPath.'ABasePresenter.php')){
            $this->outputLn('<red>ABasePresenter not exist</red>');
            return false;
        }
        $name = implode(' ',array_filter(preg_split('/(?=[A-Z])/',$reflectionClass->getShortName())));
        $menuItem = '\''.$name.'\' => \''.$reflectionClass->getShortName().':\',
            //ADD_MENU_ITEM';

        $basePresenter = file_get_contents($this->presenterPath.'ABasePresenter.php');
        if(strpos($basePresenter,'\''.$name.'\'') === false) $basePresenter = str_replace('//ADD_MENU_ITEM',$menuItem,$basePresenter);
        file_put_contents($this->presenterPath.'ABasePresenter.php',$basePresenter);
        return true;
    }

    protected function tableAddColumn($alias, $type, $additional = null)
    {
        return "\$this->addColumn('".ucfirst($alias)."', new $type('$alias'".(!empty($additional) ? ",$additional":'')."));
        ";
    }

    protected function formAddInput($type, $name, $label, $require = false, $additional = null)
    {
        $temp = "\$this->add$type('$name','$label'".(!empty($additional) ? ",$additional":'').')';
        if($require){
            $temp .= "->setRequired(_('Please fill the $label field'))";
        }
        return $temp.';
        ';
    }

    protected function createPresenter(\ReflectionClass $reflectionClass)
    {
        $template = $this->getPreparedTemplate($reflectionClass,'Presenter');
        $template = str_replace('//__ORM_UPDATED_TIMESTAMP',$this->addUpdatedTimestamp ? '$orm->updated = time();':'',$template);
        $template = str_replace('//__ORM_CREATED_TIMESTAMP',$this->addCreatedTimestamp ? '$orm->created = time();':'',$template);
        file_put_contents($this->presenterPath.$reflectionClass->getShortName().'Presenter.php',$template);
        return true;
    }

    protected function createForm(\ReflectionClass $reflectionClass)
    {
        $template = $this->getPreparedTemplate($reflectionClass,'Form');
        /** @var ABase $orm */
        $orm = $reflectionClass->newInstance();
        $form = '';
        $q = new \Query();
        $describe = $q->q('DESCRIBE '.$orm->getConfigDbTable().';')->f(\Query::FETCH_ASSOC)->ex();
        $require = [];
        foreach ($describe as $col) {
            $require[$col['Field']] = $col['Null'] == 'NO';
        }

        $skip = [];
        foreach ($orm->getAllChildAliases() as $allChildAlias) {
            $childConfig = $orm->{'getChild'.$allChildAlias.'Config'}();
            $skip[] = $childConfig->foreignKey;
        }

        foreach ($orm->getAllAliases() as $alias) {
            $additional = null;
            /** @var  $colType */
            $colConf = $orm->{'get'.ucfirst($alias).'Config'}();
            if($colConf->col == $orm->getConfigDbPrimaryKey()) continue;
            if(strpos($colConf->col,'updated') !== false) {
                $this->addUpdatedTimestamp = true;
                if(!$this->superadminForms) continue;
            }
            if(strpos($colConf->col,'created') !== false){
                $this->addCreatedTimestamp = true;
                if(!$this->superadminForms) continue;
            }
            $typeConverter = ['date' => 'Date', 'bool' => 'Checkbox'];
            if(in_array($colConf->col,$skip)){
                $type =  'Select';
                $additional = '\\'.$reflectionClass->getName().'::getForSelect()';
            }elseif(strpos($colConf->col,'created') !== false || strpos($colConf->col,'updated') !== false){
                $type = 'Timestamp';
            }elseif(is_numeric($colConf->length) && $colConf->length > 200){
                $type =  'TextArea';
            }elseif(isset($typeConverter[$colConf->type])){
                $type =  $typeConverter[$colConf->type];
            }elseif($colConf->type == 'enum'){
                $type =  'RadioList';
                $additional = '[\''.implode('\',\'',$colConf->length).'\']';
            }else{
                $type =  'Text';
            }
            $form .= $this->formAddInput($type,$colConf->col,ucfirst($colConf->alias),$require[$colConf->col],$additional);
        }
        $template = str_replace('//__ADD_COLUMNS_FORM__',$form,$template);
        file_put_contents($this->formPath.$reflectionClass->getShortName().'Form.php',$template);
        return true;
    }

    protected function createTable(\ReflectionClass $reflectionClass)
    {
        $template = $this->getPreparedTemplate($reflectionClass,'Table');
        /** @var ABase $orm */
        $orm = $reflectionClass->newInstance();
        $alias = $orm->getAlias($orm->getConfigDbPrimaryKey());
        $table = $this->tableAddColumn($alias,'\Listing\Column_Basic');

        $skip = [];
        foreach ($orm->getAllChildAliases() as $allChildAlias) {
            $childConfig = $orm->{'getChild'.$allChildAlias.'Config'}();
            $skip[] = $childConfig->foreignKey;
        }

        foreach ($orm->getAllAliases() as $alias) {
            $additional = null;
            /** @var  $colType */
            $colConf = $orm->{'get'.ucfirst($alias).'Config'}();
            if($colConf->col == $orm->getConfigDbPrimaryKey() || in_array($colConf->col,$skip)) continue;
            if(strpos($colConf->col,'created') !== false || strpos($colConf->col,'updated') !== false) {
                $type = '\Listing\Column_DateTime';
                $additional = "'d.m.Y H:i:s',true";
            }elseif($colConf->type == 'date'){
                $type = '\Listing\Column_Date';
            }elseif($colConf->type == 'bool'){
                $type = '\Listing\Column_Map';
                $additional = "[0 => 'NO', 1 => 'YES']";
            }else{
                $type = '\Listing\Column_Basic';
            }

            $table .= $this->tableAddColumn($alias,$type,$additional);
        }
        $template = str_replace('//__ADD_COLUMNS_TABLE__',$table,$template);
        file_put_contents($this->tablePath.$reflectionClass->getShortName().'Table.php',$template);
        return true;
    }

    protected function getPreparedTemplate(\ReflectionClass $reflectionClass, $templateName)
    {
        $file = __DIR__.'/ScaffoldTemplates/'.$templateName;
        if(file_exists($file.'.php')){
            $template = file_get_contents($file.'.php');
        }elseif(file_exists($file.'.latte')){
            $template = file_get_contents($file.'.latte');
        }else{
            $this->terminateWithError('Scaffold Template not found. "'.$file.'"');
        }

        $template = str_replace('__CLASS_NAME__',$reflectionClass->getShortName(),$template);
        $template = str_replace('__CLASS_NAME_LC__',strtolower($reflectionClass->getShortName()),$template);
        $template = str_replace('__CLASS_NAME_LCF__',lcfirst($reflectionClass->getShortName()),$template);
        $separatedName = implode(' ',array_filter(preg_split('/(?=[A-Z])/',$reflectionClass->getShortName())));
        $template = str_replace('__CLASS_NAME_SEPARATED__',$separatedName,$template);
        return str_replace('__ORM_CLASS_NAME__','\\'.$reflectionClass->getName(),$template);
    }

    public function getName()
    {
        return 'scaffold-nette-orm';
    }

    public function getDescription()
    {
        return 'Generator for ORM models from database table.';
    }

    public function writeHelp()
    {
        $this->outputLn("<red>General Info:</red>");
        $this->outputLn("<red>-------------</red>");
        $this->outputLn("<yellow>" . $this->getDescription() . "</yellow>");
        $this->outputLn("");
        $this->outputLn("<red>Available options:</red>");
        $this->listAvailableOptions();
        $this->outputLn(str_pad("<DB table name>", 25) . "<cyan>One or more orm names separated by space</cyan>");
        $this->outputLn("");
    }
}