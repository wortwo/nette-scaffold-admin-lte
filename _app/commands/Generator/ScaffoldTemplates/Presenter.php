<?php

namespace App\AdminModule\Presenters;
/**
 * Class __CLASS_NAME__Presenter
 */
class __CLASS_NAME__Presenter extends ABasePresenter
{
    public function renderDefault($id)
    {
        $table = new \Tables\__CLASS_NAME__List();
        $table->composeTable($this);
        $this->exportListing($table);
        $this->template->table = $table;
    }

    protected function createComponent__CLASS_NAME__Form()
    {
        $form = new \Forms\__CLASS_NAME__Form(new __ORM_CLASS_NAME__($this->getParameter('id')));
        $form->onSuccess[] = [$this, '__CLASS_NAME__FormProcess'];
        return $form;
    }

    public function __CLASS_NAME__FormProcess(\Forms\__CLASS_NAME__Form $form)
    {
        $v = $form->getValues(true);
        /** @var \ORM\ABase $orm */
        $orm = new __ORM_CLASS_NAME__($this->getParameter('id'));
        if($orm->isLoaded()){
            $action = 'edited';
            //__ORM_UPDATED_TIMESTAMP
        }else{
            $action = 'created';
            //__ORM_CREATED_TIMESTAMP
        }
        $orm->load($v);
        $orm->save();

        $this->flashMessage('__CLASS_NAME_SEPARATED__ was '.$action.' successfully', 'success');
        $this->redirect('__CLASS_NAME__:default');
    }

    public function renderDelete($id)
    {
        $orm = new __ORM_CLASS_NAME__($id);
        if(!$orm->isLoaded()){
            $this->flashMessage('__CLASS_NAME_SEPARATED__ with ID:'.$id.' not found', 'info');
            $this->redirect('__CLASS_NAME__:default');
        }
        $orm->delete();

        $this->flashMessage('__CLASS_NAME_SEPARATED__ with ID:'.$id.' was deleted', 'success');
        $this->redirect('__CLASS_NAME__:default');
    }

    public function renderDetail($id)
    {
        $orm = new __ORM_CLASS_NAME__($id);
        if(!$orm->isLoaded()){
            $this->flashMessage('__CLASS_NAME_SEPARATED__ with ID:'.$id.' not found', 'info');
            $this->redirect('__CLASS_NAME__:default');
        }
        $this->template->orm = $orm;
    }
}