<?php

namespace Tables;

use App\AdminModule\Presenters\ABasePresenter;
use Listing\Column\NetteDeleteLink;
use Listing\Column\NetteDetailLink;
use Listing\Column\NetteEditLink;
use Listing\Column_Multi;
use Listing\Connector_ObjectRelationMapper;
use Listing\Pager\Get;
use Listing\Table;

/**
 * Class __CLASS_NAME__List
 * @package Tables
 */
class __CLASS_NAME__List extends \Tables\Table
{
    public function composeTable(ABasePresenter $presenter)
    {
        /** @var \ORM\ABase $orm */
        $orm = new __ORM_CLASS_NAME__();
        $this->addClass('exportable');

        $pager = new Get((empty($presenter->getParameter('exportFormat'))) ? 20:PHP_INT_MAX, $orm->count());
        $orm->setOrderingOrder('id', $orm::ORDERING_ASCENDING);
        $orm->setOrderingLimit($pager->getLimit());
        $orm->setOrderingOffset($pager->getOffset());

        $this->addPager($pager);
        $this->addDataSource(new Connector_ObjectRelationMapper($orm->loadMultiple()));

        //__ADD_COLUMNS_TABLE__
        $actions = new Column_Multi(' ');
        $this->addClass('remove-action-column');
        $actions->addColumn(new NetteDetailLink('id', '__CLASS_NAME__:detail', $presenter));
        $actions->addColumn(new NetteEditLink('id', '__CLASS_NAME__:default', $presenter, 'UPRAVIT'));
        $actions->addColumn(new NetteDeleteLink('id', '__CLASS_NAME__:delete', $presenter, 'SMAZAT'));
        $this->addColumn('ACTION', $actions);
    }
}