<?php

namespace Forms;

class __CLASS_NAME__Form extends ABaseForm
{
    /**
     * @param \ORM\ABase|__ORM_CLASS_NAME__|null $orm
     * @param null $name
     */
    public function __construct(__ORM_CLASS_NAME__ $orm = null, $name = NULL)
    {
        parent::__construct($name);
        $action = 'Create';
        if($orm->isLoaded()){
            $action = 'Edit';
        }

        //__ADD_COLUMNS_FORM__
        if($orm->isLoaded()){
            $this->fillDefaultsFromOrm($orm);
        }
        $this->addHidden('id', $orm->primaryKey);
        $this->addSubmit('submit', $action);
    }
}