<?php

namespace CliC\Command\Generator;

use CliC\Base\ACommand;
use \CliC\Base\ICommand;
use ObjectRelationMapper\Search\Search;
use ORM\Subject;
use ORM\SubjectData;

/**
 * Class GenerateCompanyDataTypes
 * @package CliC\Command\Script
 */
class GenerateCompanyDataTypes extends ACommand implements ICommand
{
    public function __construct(\CliC\CliC $cliC)
    {
        ini_set('max_execution_time', 5000);
        set_time_limit(0);
        parent::__construct($cliC);
    }

    public function getName()
    {
        return 'generate-company-data-types';
    }

    public function getDescription()
    {
        return '';
    }

    public function writeHelp()
    {
        parent::writeHelp();
    }

    public function main()
    {
        $this->outputLn('<green>Start</green>');

        \DAO\CompanyData::generateTypes();
        
        $this->outputLn('<green>DONE</green>');
    }
}