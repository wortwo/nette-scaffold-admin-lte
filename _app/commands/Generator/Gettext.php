<?php

namespace CliC\Command\Generator;

use CliC\Base\ACommand;
use CliC\Base\ICommand;

/**
 * Class Gettext
 * @package CliC\Command\Generator
 * @property string create
 * @property string delete
 * @property boolean force
 * @property boolean binary
 * @property boolean generateDefault
 */
class Gettext extends ACommand implements ICommand
{
    protected $langDir = NULL;
    protected $defaultLang = 'cs_CZ';
    protected $otherLangs = Array('en_US', 'sk_SK', 'pl_PL');
    protected $inputDirs = Array();

    public function __construct(\Clic\CliC $cliC)
    {
        parent::__construct($cliC);
        $this->addArgument('--generate-default','generateDefault',false,'-g');
        $this->addArgument('--create-other','create',false,'-c');
        $this->addArgument('--delete-other','delete',false,'-d');
        $this->addArgument('--binary','binary',false,'-b');

        # Umisteni lang prekladu
        $this->langDir = APP_DIR . 'lang/';

        # Input Adresare
        $this->inputDirs[] = APP_DIR;
        $this->inputDirs[] = TEMP_DIR.'cache/latte/';
        if(file_exists(TEMP_DIR.'latte-temp.php')){
            $this->inputDirs[] = TEMP_DIR.'latte-temp.php';
        }
    }

    public function main()
    {
        if($this->generateDefault){
            return $this->generateDefaultFile();
        }
        if($this->delete){
            return $this->deleteLanguage();
        }
        if($this->create){
            return $this->generateFromDefaultNewLanguage();
        }
        if($this->binary){
            return $this->generateBinary();
        }

        $this->writeFullHelp();
        return true;
    }


    public function getName()
    {
        return 'gettext';
    }

    public function getDescription()
    {
        return 'Used for generating binary files and new language files (.mo, .po)';
    }

    public function writeHelp()
    {
        $this->outputLn("<red>General Info:</red>");
        $this->outputLn("<red>-------------</red>");
        $this->outputLn("<yellow>".$this->getDescription()."</yellow>");
        $this->outputLn("");
        $this->outputLn("<red>Available options:</red>");
        $this->listAvailableOptions();
        $this->outputLn("");
        $this->outputLn(str_pad("create", 25)   . "<cyan>create languages ".implode(", ", $this->otherLangs)." from default</cyan>");
        $this->outputLn(str_pad("delete", 25)   . "<cyan>delete languages ".implode(", ", $this->otherLangs)."</cyan>");
        $this->outputLn(str_pad("binary", 25)   . "<cyan>generate message.mo binary files</cyan>");
        $this->outputLn(str_pad("generate-default", 25). "<cyan>show additional info</cyan>");
        $this->outputLn();
    }

    public function writeFullHelp()
    {
        $this->writeHelp();
        $this->outputLn("<red>General Info:</red>");
        $this->outputLn("<red>-------------</red>");
        $this->outputLn("<green>Gettext</green> - Vyzaduje program POEDIT");
        $this->outputLn("<yellow>Základní nastaveni programu:</yellow>");
        $this->outputLn(str_pad("1)", 5)."Soubor->Nastavení...");
        $this->outputLn(str_pad("2)", 5).'Karta "Osobní nastavení"  => Vyplnit "Vaše jméno:", "Vaše emailová adresa:"');
        $this->outputLn(str_pad("3)", 5).'Potvrdit "OK"');
        $this->outputLn("<yellow>Development nastavení programu - první otevření PO souboru:</yellow>");
        $this->outputLn(str_pad("1)", 5).'Karta "Prohledávané cesty" - "Základní cesta:" a v "Cesty" vytvořit novou položku a do obou vyplnit cestu k _app (př: Z:\fastcash2\_app)');
        $this->outputLn(str_pad("2)", 5).'Potvrdit "OK"');
        $this->outputLn("<yellow>Prvni vytvoreni lang souboru:</yellow>");
        $this->outputLn(str_pad("1)", 5).'vytvoreni defaultniho Lang souboru -  php clic.php generator:gettext');
        $this->outputLn("<yellow>Pridani noveho jazyka:</yellow>");
        $this->outputLn(str_pad("1)", 5).'pokud je defaultni jazyk prekladan/upravovan je nutne jej aktualizovat viz. "Aktualizace langoveho souboru", jinak bod 1) v "Prvni vytvoreni lang souboru"');
        $this->outputLn(str_pad("2)", 5).'vytvoreni noveho jazyka (parameter LANGUAGE musi byt ve formatu:[a-z]{2}_[A-Z]{2} priklad: en_US ) -  php clic.php generator:gettext --create=[LANGUAGE]');
        $this->outputLn(str_pad("3)", 5).'otevreni souboru _app/lang/[LANGUAGE]/LC_MESSAGES/messages.po v POEDIT');
        $this->outputLn(str_pad("4)", 5).'vytvorit preklady v POEDIT a ulozit');
        $this->outputLn(str_pad("5)", 5).'pokud neni v POEDITu nastaveno generovani do MO souboru - php clic.php generator:gettext --binary');
        $this->outputLn("<yellow>Aktualizace langoveho souboru:</yellow>");
        $this->outputLn(str_pad("1)", 5).'otevreni souboru _app/lang/[LANGUAGE]/LC_MESSAGES/messages.po v POEDIT');
        $this->outputLn(str_pad("2)", 5).'kliknuti na "Aktualizovat"');
        $this->outputLn(str_pad("3)", 5).'prelozit zvyraznene klice');
        $this->outputLn();
    }

    protected function getLangMessageFilePath($lang)
    {
        return $this->langDir . $lang . '/LC_MESSAGES/messages.po';
    }

    protected function generateDefaultFile()
    {
        $iD = implode(' ', $this->inputDirs);
        $oF = $this->getLangMessageFilePath($this->defaultLang);

        $this->output('Generating latte cache into file: '.TEMP_DIR . 'latte-temp.php');
        $matches = Array();
        preg_match_all('/{_(.*)}/i', `cat $(find $iD -regextype posix-awk -regex "(.*.phtml)") 2> /dev/null`, $matches);
        $recompile = preg_replace('/{_n(.*?)}/i', 'ngettext(\1);', implode("\n", $matches[0]));
        $recompile = preg_replace('/{_dn(.*?)}/i', 'ngettext(\1);', $recompile);
        $recompile = preg_replace('/{_d(.*?)}/i', 'ngettext(\1);', $recompile);
        $recompile = preg_replace('/{_(.*?)}/i', 'gettext(\1);', $recompile);
        file_put_contents(TEMP_DIR . 'latte-temp.php', "<?php\n" . $recompile);
        $this->sendOk();
        $this->outputLn();

        $this->output('Generating default file: '. $oF);
        if(!file_exists($oF)){
            $oP = str_replace('messages.po','',$oF);
            `mkdir -p $oP && touch $oF`;
        }
        `find $iD -regextype posix-awk -regex "(.*.phtml|.*.php)" | xargs xgettext -j --from-code=UTF-8 --language=PHP --output=$oF`;
        $this->sendOk();
        $this->outputLn();
        return true;
    }

    protected function generateFromDefaultNewLanguage()
    {
        foreach($this->otherLangs as $lang){
            $this->output('Generating '.$this->getLangMessageFilePath($lang));
            if(!is_file($this->getLangMessageFilePath($lang))){
                mkdir(dirname($this->getLangMessageFilePath($lang)), 0777, true);
                copy($this->getLangMessageFilePath($this->defaultLang), $this->getLangMessageFilePath($lang));
                $this->sendOk();
            } else {
                $this->sendFailExplain('File exists');
            }
            $this->outputLn();
        }

        return true;
    }

    /**
     * Vygeneruje binarni soubory ke vsem existujicim jazykum
     * @return bool
     */
    protected function generateBinary()
    {
        foreach(array_merge(Array($this->defaultLang), $this->otherLangs) as $lang){
            $lF = $this->getLangMessageFilePath($lang);
            $dir = dirname($lF);
            $this->output('Generating .mo file for: '.$lF);
            if(is_file($lF)){
                `(cd $dir && rm -rf ./*.mo && msgfmt $lF)`;
                $this->sendOk();
            } else {
                $this->sendFail();
                $this->output(' no such file');
            }
            $this->outputLn();
        }

        return true;
    }

    /**
     * Smaze vsechny ostatni jazyky
     * @return bool
     */
    protected function deleteLanguage()
    {
        foreach($this->otherLangs as $lang){
            $dir = APP_DIR . 'lang/'.$lang;
            $this->output('Removing '.$dir);
            `rm -rf $dir`;
            $this->sendOk();
            $this->outputLn();
        }

        return true;
    }



}