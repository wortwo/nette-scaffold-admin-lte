<?php

namespace CliC\Command\Generator;

use CliC\Base\ACommand;
use \CliC\Base\ICommand;
use CliC\Base\Input;
use CliC\CliC;

/**
 * Class MigrationFromORM
 * @package CliC\Command\Generator
 *
 * @property string $ormPath
 * @property string $migrationsPath
 * @property boolean $all
 */
class MigrationFromOrm extends ACommand implements ICommand
{
    use Input;
    public function __construct(CliC $cliC)
    {
        parent::__construct($cliC);
        $this->addArgument('--all', 'all', true);
        $this->addArgument('--orm-path=(.*)', 'ormPath', ORM_DIR);
        $this->addArgument('--migrations-path=(.*)', 'migrationsPath', BASE_DIR . '_migrations/');
    }

    public function getName()
    {
        return 'migration-from-orm';
    }

    public function getDescription()
    {
        return '';
    }

    public function writeHelp()
    {
        parent::writeHelp();
    }

    public function main()
    {
        if (!IS_DEVEL) {
            $this->terminateWithError('Can be run only in devel environment!');
        }

        $this->checkPaths('ormPath','migrationsPath');

        $this->outputLn('<green>Start</green>');

        /** @var \ReflectionClass[] $reflections */
        $reflections = [];
        $ormNames = $this->filterUnrecognizedOptions();
        if($this->all){
            $ormNames = array_merge($ormNames,['ABase','.', '..']);
            $reflections = $this->scandirForORM($this->ormPath,$ormNames);
        }else{
            foreach ($ormNames as $ormName) {
                $ref = $this->getReflectionByClassName($ormName);
                if(!$ref) continue;
                $reflections[] = $ref;
            }
        }

        if(empty($reflections)) $this->terminateWithError('No orm found! Check paths and orm names.');

        $migration = new \ObjectRelationMapper\Migration\Builder();

        foreach ($reflections as $reflection) {
            $migration->addORMToWatch($reflection->newInstance());
        }

        $output = null;
        if(!$migration->areDifferent()) {
            $this->outputLn('<green>No difference found. DONE.</green>');
            return;
        }
        $this->outputLn('<red>Output migration file:</red>');
        $output = $migration->getDiff();
        echo $output;
        $this->outputLn();
        $this->outputLn();
        if($this->askUserYesNo('Do you want to save migration file?')){
            $unique = '';
            if(preg_match('/.*(AutoGeneratedMigration([a-z0-9]+)).*/',$output,$matches)){
                $unique = $matches[2];
            }
            $file = $this->migrationsPath . date('YmdHis') . '_auto_generated_migration'.$unique.'.php';
            file_put_contents($file,trim($output));
            $this->outputLn('<yellow>Migration file created: </yellow><blue>'.$file.'</blue>');
        }
        $this->outputLn('<green>DONE.</green>');
    }

    protected function checkPaths()
    {
        foreach (func_get_args() as $path) {
            if (!file_exists($this->{$path}) || !is_dir($this->{$path})) {
                $this->terminateWithError('Incorect '.$path.' parameter. Must be valid dir path: '.$this->{$path});
            }
        };
    }

    protected function scandirForORM(string $path, Array $exclude, string $namespace = '')
    {
        $result = [];
        foreach (array_diff(scandir($path), $exclude) as $file) {
            if (is_dir($path . "/" . $file)) {
                $result = array_merge($result, $this->scandirForORM($path . "/" . $file, $exclude, $file . '\\'));
            } else {
                $ref = $this->getReflectionByClassName('\ORM\\' . current(explode('.', $namespace . $file)));
                if(!$ref) continue;
                $result[] = $ref;
            }
        }
        return $result;
    }

    protected function getReflectionByClassName(string $className)
    {
        if(!class_exists($className)) return false;
        $ref = new \ReflectionClass($className);
        if(!$ref->isInstantiable() || !$ref->isSubclassOf('\ORM\ABase')) return false;
        $this->outputLn('   <blue>'.$className.'</blue>');
        return $ref;
    }
}
