<?php

namespace CliC\Command\Generator;

use \CliC\Base\ICommand;
use Nette\DI\Config\Loader;
use ObjectRelationMapper\Connector\PDO;
use ObjectRelationMapper\Generator\DbToOrm;

/**
 * Class GenerateOrmFromDb
 * @package CliC\Command
 *
 * @property string database
 * @property string namespace
 * @property boolean all
 * @property string dbServer
 * @property string colPrefix
 * @property string extends
 */
class GenerateOrmFromDb extends \CliC\Base\ACommand implements ICommand
{
    /**
     * @var Array
     */
    protected $config;
    /**
     * @var PDO
     */
    protected $connection;

    public function __construct(\CliC\CliC $cliC)
    {
        parent::__construct($cliC);
        $loader = new Loader();
        $this->config = $loader->load(APP_DIR . 'config/config.devel.neon')['parameters']['database']['master'];
        $this->addArgument('--all','all',false);
        $this->addArgument('--database=(.*)','database',$this->config['name']);
        $this->addArgument('--namespace=(.*)','namespace','ORM');
		$this->addArgument('--db-server=(.*)','dbServer','master');
        $this->addArgument('--col-prefix=(.*)', 'colPrefix', null);
        $this->addArgument('--extends=(.*)', 'extends', 'ABase');
    }

    public function main()
    {
        if(!IS_DEVEL){
            return $this->outputLn("<red>Can be run only in devel environment!</red>");
        }
        try {
            $this->connection = new PDO(new \PDO('mysql:host=' . $this->config['host'] . ';dbname=' . $this->database . '', $this->config['user'], $this->config['pass']));
        } catch (\Exception $e) {
            $this->terminateWithError('Database with name "' . $this->database . '" not exist');
        }
        $tableNames = $this->getTableNameArray();
        foreach($tableNames as $tableName) {
            $objectName = $this->getOrmNameFromTableName($tableName);
            $this->outputLn("<green>Generating ORM object '" . $objectName . "'...</green>");
            try {
                new DbToOrm($this->connection, $tableName, $this->dbServer, $objectName, $this->extends, APP_DIR . 'models/' . $this->namespace, $this->colPrefix, $this->namespace);
            } catch (\Exception $e) {
                $this->outputLn("<red>Table with name '" . $tableName . "' not exist in database '" . $this->config['name'] . "'</red>");
                continue;
            }
        }
        $this->outputLn("<cyan>".count($tableNames)." ORM objects created</cyan>");
    }

    /**
     * Return array of table names from database if option "--all" = true or from command argument
     * @return array
     */
    protected function getTableNameArray()
    {
        if(!$this->all){
            return $this->filterUnrecognizedOptions();
        }
        $tables = $this->connection->query("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='".$this->database."';",Array(),null);
        $tables = iterator_to_array(new \RecursiveIteratorIterator(new \RecursiveArrayIterator($tables)), 0);
        if(($key = array_search('phinxlog', $tables)) !== false) {
            unset($tables[$key]);
        }
        return $tables;
    }

    /**
     * Return ORM object name from DB table name
     * @param $tableName
     * @return string
     */
    protected function getOrmNameFromTableName($tableName)
    {
        return implode('',array_map('ucfirst',explode('_',$tableName)));
    }

    public function getName()
    {
        return 'generate-orm-from-db';
    }

    public function getDescription()
    {
        return 'Generator for ORM models from database table.';
    }

    public function writeHelp()
    {
        $this->outputLn("<red>General Info:</red>");
        $this->outputLn("<red>-------------</red>");
        $this->outputLn("<yellow>".$this->getDescription()."</yellow>");
        $this->outputLn("");
        $this->outputLn("<red>Available options:</red>");
        $this->listAvailableOptions();
        $this->outputLn(str_pad("<DB table name>", 25)."<cyan>One or more table names separated by space</cyan>");
        $this->outputLn("");
    }
}