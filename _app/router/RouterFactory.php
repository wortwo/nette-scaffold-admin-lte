<?php

namespace App;

use Nette;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{
	use Nette\StaticClass;

	/**
	 * @return Nette\Application\IRouter
	 */
	public static function createRouter()
	{
        $router = new RouteList();

        $router[] = new Route('//www.%domain%/<presenter>/<action=default>/[<id>/]', [
            'module'    => 'Www',
            'presenter' => 'Default',
            'action'    => 'default'
        ]);

        $router[] = new Route('//admin.%domain%/<presenter>/<action=default>/[<oid>/]', [
            'module'    => 'Admin',
            'presenter' => 'Default',
            'action'    => 'default'
        ]);

        return $router;
	}

}
