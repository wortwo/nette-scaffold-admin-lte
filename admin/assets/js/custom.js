$(function () {
    $('.dataTable').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
    $('.datetimepicker').datetimepicker({
        locale: 'cs',
    });
    $('.datepicker').datepicker({
        locale: 'cs',
    });
    $(".birthday-picker").birthdayPicker({
        maxAge : 110,
        minAge : 18,
        maxYear : new Date().getFullYear(),
        "dateFormat" : "littleEndian",
        "monthFormat" : "number",
        "placeholder" : true,
        "defaultDate" : false,
        "sizeClass"	: "span1",
    });
});

function getUrlParams(param) {
    var vars = {};
    window.location.href.replace( location.hash, '' ).replace(
        /[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
        function( m, key, value ) { // callback
            vars[key] = value !== undefined ? value : '';
        }
    );
    if ( param ) {
        return vars[param] ? vars[param] : null;
    }
    return vars;
}

$(document).ready(function() {
    var flash = $('.alert');
    flash.delay(5000).fadeOut('slow');
    flash.click().fadeOut('slow');

    $('.exportable').each(function(){
        $(this).parent().parent().prev().children().first().html(
            "<select class='form-control col-sm-1' style='width: auto;' id='export-format' name='exportFormat'>" +
            "<option value=''>Export</option>" +
            "<option value='XLS'>MS Excel</option>" +
            "<option value='CSV'>CSV</option>" +
            "<option value='PDF'>PDF</option>" +
            //"<option value='TXT'>TXT</option>" +
            //"<option value='JSON'>JSON</option>" +
            //"<option value='HTML'>HTML</option>" +
            "</select>"
        );
    });

    $('#export-format').change(function () {
        var params = getUrlParams();
        params['exportFormat'] = $(this).val();
        params = jQuery.param(params);
        window.location = window.location.protocol + '//' + window.location.hostname + window.location.pathname + '?' + params;
    });
});

$(document).on('click', '.btn-danger', function(e) {
    if(!window.confirm('Opravdu smazat?')){
        e.preventDefault();
    }
});